from gpiozero import LED, Button
from time import sleep

#led = LED(17)

led0 = LED(10)
ledA = LED(9)
ledB = LED(11)
ledC = LED(0)
ledD = LED(5)
ledE = LED(6)
ledF = LED(13)
ledG = LED(19)
btnArm = Button(2)
btnZ1 = Button(3)
btnZ2 = Button(4)
btnZ3 = Button(17)
btnZ4 = Button(27)


armed = False

num = {
    '0':(0,0,0,0,0,0,1),
    '1':(1,0,0,1,1,1,1),
    '2':(0,0,1,0,0,1,0),
    '3':(0,0,0,0,1,1,0),
    '4':(1,0,0,1,1,0,0),
    '5':(0,1,0,0,1,0,0),
    '6':(0,1,0,0,0,0,0),
    '7':(0,0,0,1,1,1,1),
    '8':(0,0,0,0,0,0,0),
    '9':(0,0,0,0,1,0,0),
    '10':(0,0,0,1,0,0,0),
    '11':(1,1,1,1,1,1,0)
    }


def nombre(i,tmp):
    value = str(i)
    print(value)
 #   led.on()   
    ledA.value = num[value][0]
    ledB.value = num[value][1]
    ledC.value = num[value][2]
    ledD.value = num[value][3]
    ledE.value = num[value][4]
    ledF.value = num[value][5]
    ledG.value = num[value][6]
    sleep(tmp)
  #  led.off()


#if __name__=="__main__":
alertZone = 0
while True:
    if btnArm.value == 0 and armed == False:
        nombre(0,0.3)
    else:
        if armed == False:
            for i in range(0,11):
                nombre(i,1)
                print('Armed')
            armed = True
            
        if btnZ1.value == 1:
            alertZone = 1    
        elif btnZ2.value == 1:
            alertZone = 2
        elif btnZ3.value == 1:
            alertZone = 3
        elif btnZ4.value == 1:
            alertZone = 4
        
        if alertZone != 0:
            for i in range(10,0,-1):
                nombre(i,1)
            print('Alert')        

            while True:
                nombre(alertZone,0.5)
                nombre(11,0.5)
                if btnZ1.value == 1 and alertZone==1:
                    alertZone = 0
                    armed = False
                    break
                elif btnZ2.value == 1 and alertZone==2:
                    alertZone = 0
                    armed = False
                    break
                elif btnZ3.value == 1 and alertZone==3:
                    alertZone = 0
                    armed = False
                    break
                elif btnZ4.value == 1 and alertZone==4:
                    alertZone = 0
                    armed = False
                    break
                

